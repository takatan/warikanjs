import { atom } from 'recoil';

import { addEntry, defaultRootState } from './models/RootState';

export const rootState = atom({
  key: 'rootState',
  default: addEntry(1000, 1)(defaultRootState),
});
