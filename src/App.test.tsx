import { fireEvent, render } from '@testing-library/react';
import { describe, expect, it } from 'vitest';

import App from './App';

it('renders without crashing', () => {
  expect(() => {
    render(<App />);
  }).not.toThrow();
});

describe('<App/>', () => {
  it('test', () => {
    const app = render(<App />);
    expect(app.container.querySelector('#total')?.textContent).toBe('1000');
    expect(app.container.querySelector('#total-number')?.textContent).toBe('1');
    const plus = app.container.querySelector('button.or-plus-button');
    if (plus !== null) fireEvent.click(plus);
    expect(app.container.querySelector('#total')?.textContent).toBe('2000');
  });
});
