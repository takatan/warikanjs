import { evolve } from 'effect/Struct';

export interface EntryState {
  amount: number;
  num: number;
}

const modify =
  (diff: number) =>
  (x: number): number =>
    Math.max(x + diff, 0);

export const modifyAmount = (diff: number): ((s: EntryState) => EntryState) =>
  evolve({ amount: modify(diff) });

export const modifyNumber = (diff: number): ((s: EntryState) => EntryState) =>
  evolve({ num: modify(diff) });

export const entryTotal = (self: EntryState): number => self.num * self.amount;
