import { pipe } from 'effect';
import { append, map, modify, remove } from 'effect/Array';
import { sumAll } from 'effect/Number';
import { evolve } from 'effect/Struct';

import {
  type EntryState,
  entryTotal,
  modifyAmount,
  modifyNumber,
} from './EntryState';

export const addEntry = (
  amount: number,
  number_: number,
): ((s: RootState) => RootState) =>
  evolve({ entries: (x) => append({ amount, num: number_ })(x) });

export const defaultRootState: RootState = {
  entries: [],
};

export interface RootState {
  entries: EntryState[];
}

export const delEntry = (index: number): ((s: RootState) => RootState) =>
  evolve({ entries: (x) => remove(index)(x) });

export const modifyAmountAt = (
  index: number,
  diff: number,
): ((s: RootState) => RootState) =>
  evolve<RootState, RootState>({
    entries: modify(index, modifyAmount(diff)),
  });

export const modifyNumberAt = (
  index: number,
  diff: number,
): ((s: RootState) => RootState) =>
  evolve<RootState, RootState>({
    entries: modify(index, modifyNumber(diff)),
  });

export const total = (entryStates: EntryState[]): number =>
  pipe(entryStates, map(entryTotal), sumAll);

export const totalNumber = (entryStates: EntryState[]): number =>
  pipe(
    entryStates,
    map((x) => x.num),
    sumAll,
  );
