import './App.css';

import { createTheme, ThemeProvider } from '@mui/material';
import type * as React from 'react';
import { StrictMode } from 'react';
import { RecoilRoot } from 'recoil';

import { WarikanApp } from './components/Warikan';

const theme = createTheme({});

const App = (): React.ReactElement => (
  <StrictMode>
    <ThemeProvider theme={theme}>
      <RecoilRoot>
        <WarikanApp />
      </RecoilRoot>
    </ThemeProvider>
  </StrictMode>
);
export default App;
