import { Button } from '@mui/material';
import type * as React from 'react';
import { useSetRecoilState } from 'recoil';

import { modifyAmountAt } from '../models/RootState';
import { rootState } from '../state';

interface IProperties {
  index: number;
  diff: number;
}

const ModifyAmount: React.FC<IProperties> = ({ index, diff }) => {
  const setRoot = useSetRecoilState(rootState);
  const icon = (diff > 0 ? '+' : '') + diff.toString();
  return (
    <Button
      variant="outlined"
      disableRipple={true}
      className="or-modify-amount"
      onClick={() => {
        setRoot(modifyAmountAt(index, diff));
      }}
      href=""
    >
      {icon}
    </Button>
  );
};

export default ModifyAmount;
