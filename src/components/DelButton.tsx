import { Delete } from '@mui/icons-material';
import { Button } from '@mui/material';
import type * as React from 'react';
import { useSetRecoilState } from 'recoil';

import { delEntry } from '../models/RootState';
import { rootState } from '../state';

interface IStateProperties {
  index: number;
}

const DelButton: React.FC<IStateProperties> = ({ index }) => {
  const setRoot = useSetRecoilState(rootState);

  return (
    <Button
      color="primary"
      className="or-trash-button"
      onClick={() => {
        setRoot(delEntry(index));
      }}
      href=""
    >
      <Delete />
    </Button>
  );
};

export default DelButton;
