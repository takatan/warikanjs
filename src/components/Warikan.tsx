import { Add } from '@mui/icons-material';
import { Fab, Table, TableBody } from '@mui/material';
import type * as React from 'react';
import { useRecoilState } from 'recoil';

import { addEntry, total, totalNumber } from '../models/RootState';
import { rootState } from '../state';
import TableContent from './TableContent';

export const WarikanApp: React.FC = () => {
  const [root, setRoot] = useRecoilState(rootState);

  const { entries } = root;
  const amount = total(entries);
  const number_ = totalNumber(entries);
  return (
    <div className="App">
      <div className="total-line">
        合計: <span id="total">{amount}</span>円 ({' '}
        <span id="total-number">{number_}</span> 人)
      </div>
      <Table>
        <TableBody>
          <TableContent />
        </TableBody>
      </Table>
      <div>
        <Fab
          href=""
          color="secondary"
          onClick={() => {
            setRoot(addEntry(1000, 1));
          }}
          className="or-plus-button"
          size="small"
        >
          <Add />
        </Fab>
      </div>
    </div>
  );
};
