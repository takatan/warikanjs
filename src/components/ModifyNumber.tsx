import { Button } from '@mui/material';
import type * as React from 'react';
import { useSetRecoilState } from 'recoil';

import { modifyNumberAt } from '../models/RootState';
import { rootState } from '../state';

interface IStateProperties {
  index: number;
  diff: number;
}

const ModifyNumber: React.FC<IStateProperties> = ({ index, diff }) => {
  const setRoot = useSetRecoilState(rootState);

  const icon = (diff > 0 ? '+' : '') + diff.toString();
  return (
    <Button
      variant="outlined"
      disableRipple={true}
      className="or-modify-number"
      onClick={() => {
        setRoot(modifyNumberAt(index, diff));
      }}
    >
      {icon}
    </Button>
  );
};

export default ModifyNumber;
