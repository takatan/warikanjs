import { Paper, TableCell, TableRow } from '@mui/material';
import { map } from 'effect/Array';
import * as React from 'react';
import { useRecoilValue } from 'recoil';

import * as E from '../models/EntryState';
import { rootState } from '../state';
import DelButton from './DelButton';
import ModifyAmount from './ModifyAmount';
import ModifyNumber from './ModifyNumber';

const TableContent: React.FC = () => {
  const root = useRecoilValue(rootState);

  return (
    <>
      {map((entryState: E.EntryState, index: number) => (
        <React.Fragment key={index}>
          <TableRow>
            <TableCell className="or-amount-column">
              <ModifyAmount index={index} diff={-500} />
            </TableCell>
            <TableCell className="or-amount-column">
              <ModifyAmount index={index} diff={-100} />
            </TableCell>
            <TableCell className="or-amount-column" rowSpan={2}>
              <Paper>
                <div className="amount-line">{entryState.amount}円</div>
                <div className="number-line">{entryState.num}人</div>
              </Paper>
            </TableCell>
            <TableCell className="or-amount-column">
              <ModifyAmount index={index} diff={100} />
            </TableCell>
            <TableCell className="or-amount-column">
              <ModifyAmount index={index} diff={500} />
            </TableCell>
            <TableCell className="or-subtotal-column">
              <DelButton index={index} />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className="or-number-column">
              <ModifyNumber index={index} diff={-5} />
            </TableCell>
            <TableCell className="or-number-column">
              <ModifyNumber index={index} diff={-1} />
            </TableCell>
            <TableCell className="or-number-column">
              <ModifyNumber index={index} diff={1} />
            </TableCell>
            <TableCell className="or-number-column">
              <ModifyNumber index={index} diff={5} />
            </TableCell>
            <TableCell className="or-subtotal-column">
              <div className="subtotal-box">
                <span className="subtotal-line">
                  {E.entryTotal(entryState)} 円
                </span>
              </div>
            </TableCell>
          </TableRow>
        </React.Fragment>
      ))(root.entries)}
    </>
  );
};

export default TableContent;
