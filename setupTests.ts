// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

import { TextEncoder } from 'node:util';

// https://zenn.dev/ocknamo/scraps/f0de16520e7117
// https://github.com/inrupt/solid-client-authn-js/issues/1676#issuecomment-917016646
// Polyfill for encoding which isn't present globally in jsdom
if (globalThis.TextEncoder === undefined) {
  globalThis.TextEncoder = TextEncoder;
}

// if (typeof global.TextDecoder === 'undefined') {
//   global.TextDecoder = TextDecoder;
// }
